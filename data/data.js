const data = {
	reports: [
		{
			date: 'Nov 22, 2018',
			dailyReports: [
				{ clockedInTime: '09:00:00', clockedOutTime: '09:55:00', sum: '00:55:00' },
				{ clockedInTime: '10:00:00', clockedOutTime: '13:45:00', sum: '03:45:00' },
			],
			total: '04:40:00',
		},
	],
	previousClockedInDate: 'Nov 22, 2018',
};

module.exports = data;