const Sequelize = require('sequelize');
const config = require('config');

const sequelize = new Sequelize(
	config.get('database.mysql.database'),
	config.get('database.mysql.user'),
	config.get('database.mysql.password'),
	{
		logging: false, // eslint-disable-line
		dialect: 'mysql',
		host: config.get('database.mysql.host'),
		port: config.get('database.mysql.port'),
		define: {
			paranoid: true,
		},
	}
);

module.exports = sequelize;
