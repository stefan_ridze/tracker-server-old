const UnprocessableEntityError = require('../exceptions/unprocessableEntity');
const BaseError = require('../exceptions/');

module.exports = function error(err, req, res, next) {
	if (err instanceof Error) {
		// @ts-ignore

		if (!err.code) {
			let errorObject = null;

			// Improve this.
			if (err.message.search('invalid input syntax') !== -1 ||
				err.message === 'Validation Error' ||
				err.message === 'Validation error' ||
				err.message.search('value too long') !== -1 ||
				err.message.search('data must be a string and salt') !== -1 ||
				err.message.search('Invalid value') !== -1 ||
				err.message.search('operator does not exist') !== -1 ||
				err.message.search('data and hash must be strings') !== -1 ||
				err.message.search('expression is of type') !== -1 ||
				err.message.search('Validation error:') !== -1 ||
				err.message.search('invalid input value') !== -1 ||
				err.message.search('insert or update on table') !== -1 ||
				err.message.search('string violation') !== -1) {
				errorObject = new UnprocessableEntityError('Validation error', err.errors || err.message);
				errorObject.stack = err.stack;
			}

			if (errorObject !== null) {
				return res.status(errorObject.code).json(errorObject);
			}
		}

		let {
			code,
		} = err;

		code = Number.isInteger(code) && code > 0 ? code : 500;

		Object.assign(err, {
			toJSON: BaseError.prototype.toJSON,
			code,
		});

		return res.status(code).json(err);
	}

	return next(null, req, res, next);
};
