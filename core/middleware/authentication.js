const {
	UserService,
	OAuthAccessTokenService,
} = require('../services');

const fillLocalsNull = (resLocals) => {
	resLocals.user = null;
	resLocals.accessToken = null;
}

async function populateUser(req, res, next) {
	const accessToken = req.headers.token;
	// @TODO:stefan Populate user samo puni podatke, nema greske, moze samo da setuje prazne podatke ako nema tokena
	if (accessToken) {
		const tokenIsValid = await OAuthAccessTokenService.findUserIdByAccessToken(accessToken);
		if (tokenIsValid) {
			const { userId } = tokenIsValid;
			const user = await UserService.findUserById(userId);
			if (user) {
				res.locals.user = user;
				res.locals.accessToken = accessToken;
				return next();
			}
			fillLocalsNull(res.locals);
			return next();
		}
		fillLocalsNull(res.locals);
		return next();
	}
	fillLocalsNull(res.locals);
	return next();
}

module.exports = populateUser;
