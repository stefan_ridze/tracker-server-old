const UnauthorizedError = require( '../exceptions/unauthorized');

const hasToken = (req, res, next) => {
	if (res.locals.accessToken) {
		return next();
	}
	return next(new UnauthorizedError());
};

module.exports = hasToken;

