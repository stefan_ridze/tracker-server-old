const NotImplementedError = require('../exceptions/notimplemented');

module.exports = function fomatter(req, res, next) {
	res.format({
		default() {
			return next(new NotImplementedError());
		},
		json() {
			if (res.locals.data && res.locals.data.rows) {
				if (req.query.encapsulated) {
					return res.json({
						count: res.locals.data.count,
						data: res.locals.data.rows,
					});
				}

				res.append('X-Count', res.locals.data.count);
				return res.json(res.locals.data.rows);
			}

			return res.json(res.locals.data);
		},
		'image/jpeg': function () { // eslint-disable-line
			return res.send(res.locals.data);
		},
	});
};
