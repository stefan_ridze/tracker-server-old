const config = require('config');

class BaseError extends Error {
	constructor(message, description) {
		super(message);

		this.message = message;
		this.description = description;

		if (config.get('application.errors.stack')) {
			Error.captureStackTrace(this, this.constructor);
		}
	}

	toJSON() {
		return {
			code: this.code,
			message: this.message,
			description: this.description,
			stack: (this.stack || '').split('\n'),
		};
	}
}

BaseError.stackTraceLimit = 20;

module.exports = BaseError;
