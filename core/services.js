const glob = require('glob');
const path = require('path');
const capitalizeFirstLetter = require('../helpers/capitalizeFirstLetter');

const services = {};
// store constant values

const s = glob.sync('**/*.js', {
	cwd: path.resolve(`${__dirname}/../services`),
});

for (let i = 0; i < s.length; i += 1) {
	const service = s[i];
	if (service !== 'index.js') {
		const requireService = require(`${__dirname}/../services/${service}`); // eslint-disable-line
		const splitedName = service.split('/');
		const name = splitedName[(splitedName.length - 1)].split('.')[0];
		services[`${capitalizeFirstLetter(name)}Service`] = requireService;
	}
}

// Initialize services with their relations
Object.keys(services).forEach((key) => {
	if (typeof services[key].initialize !== 'undefined') {
		services[key].initialize(services);
	} else {
		console.log(key, 'Has no initialize');
	}
});

module.exports = services;
