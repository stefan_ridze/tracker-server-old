const config = require('config');
const express = require('express');
const bodyParser = require('body-parser');
const glob = require('glob');
const path = require('path');
const cors = require('cors');
const populateUser =  require('./middleware/authentication');

const error = require('./middleware/error');
const formatter = require('./middleware/formatter');

const app = express();
let localCallback = () => {};

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((request, response, next) => {
	response.header('Access-Control-Allow-Origin', 'http://192.168.0.37:19001');
	response.header('Access-Control-Expose-Headers', 'token'); // !!! cannot get token on frontend without this
	response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	response.header('Access-Control-Allow-Credentials', true);
	next();
});

// @TODO:Uvek puni autentikovanog usera ako ima token koji je validan, ako nema gresku bacas samo na rutama koje traze da je autentikovano
app.use((req, res, next) => populateUser(req, res, next));


glob('**/*.js', {
	cwd: path.resolve(`${__dirname}/../controllers/`),
}, (err, files) => {
	files.forEach((file) => {
		const controller = require(`${__dirname}/../controllers/${file}`); // eslint-disable-line
		if (typeof controller === 'function') {
			app.use('/api', controller);
		}
	});

	app.use(formatter);
	app.use(error);

	const port = config.get('application.port') || 5000;

	app.listen(port, () => {
		console.log(`Listening on port ${port}`);
		localCallback();
	});
});

module.exports = (callback) => {
		localCallback = callback;
};
