const apiTest = require('./api');
const server = require('./shared/startServer.test');

describe('Time-Tracker Tests', () => {

	describe('Run Server', () => {
		server();
	});
	describe('Run tests', () => {
		apiTest();
	});
});
