const chai = require('chai');
const chaiHttp = require('chai-http');
const chaiAsPromised = require('chai-as-promised');
const supertest = require('supertest');

chai.use(chaiHttp);
chai.use(chaiAsPromised);

const port = process.env.NODE_ENV !== 'test' ? 8000 : 19999;
const request = supertest({
	address: () => ({
		port,
	}),
});


module.exports = {
	host: 'http://localhost:19999',
	chai,
	expect: chai.expect,
	request,
};