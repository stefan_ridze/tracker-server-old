class Config {
	static generateEmail() {
		return `user${Math.floor(Math.random() * 1000000)}.${Date.now()}@time-tracker.com`;
	}

	static generatePassword() {
		return `${Math.floor(Math.random() * 10000)}.password`;
	}

	static generateFullName() {
		return `full${Math.floor(Math.random() * 10000)}.Name`;
	}

	static generateReport() {
		return {
			clockedInTime: '17:26:14',
			clockedOutTime: '17:34:09',
			sum: '00:07:55',
			date: 'Dec 7, 2018',
		}
	}
}

module.exports = Config;
