class UserStorage {
	constructor() {
		this.reset();
	}

	reset() {
		this.data = {};
		this.data.reports = [];
	}

	getUser() {
		if (!this.data.user) {
			throw new Error('Missing user data');
		}
		return this.data.user;
	}

	setUser(user) {
		this.data.user = user;
	}

	getUserSignInCredentials() {
		if (!this.data.user) {
			throw new Error('Missing user data');
		}
		const {
			email,
			password,
		} = this.data.user;
		return {
			email,
			password,
		};
	}

	addNewReport(report) {
		this.data.reports.push(report);
	}

	getReports() {
		return this.data.reports;
	}

	setUserToken(token) {
		this.data.user.token = token;
	}

	getUserToken() {
		if (!this.data.user.token) {
			throw new Error('Missing token');
		};
		return this.data.user.token;
	}
}

module.exports = UserStorage;