const app = require('../../core/server');

module.exports = () => {
	it('Starts the server', (done) => {
		app(setTimeout(() => done(), 0));
	});
};