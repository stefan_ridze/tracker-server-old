const UserStorage = require('../system/UserStorage');
const Config = require('../../tests/system/config');

const signUp = require('../api/user/signUp');
const signIn = require('../api/user/signIn');
const submitReport = require('../api/user/submitReport');
const getReports = require('../api/user/getReports');

module.exports = () => {
	describe('Full user lifecycle trough the api', async () => {
		const newUser = new UserStorage();

		signUp(newUser);
		signIn(newUser);
		submitReport(newUser, Config.generateReport());
		getReports(newUser);

	});
};
