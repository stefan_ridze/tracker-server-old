const { expect, request } = require('../../');

module.exports = (user) => {
	it('GET /api/reports', (done) => {
		request.get('/api/reports')
			.set('token', user.getUserToken())
			.send()
			.expect(200)
			.then((res) => {
				expect(res.body).to.have.property('');
				console.log('controller returned this:', res.body);
				return done();
			})
			.catch(done);
	});
};