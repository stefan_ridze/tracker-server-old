const { expect, request } = require('../../');

module.exports = (user) => {
	it('POST /api/users/sign-in', (done) => {
		request.post('/api/users/sign-in')
			.send(user.getUserSignInCredentials())
			.expect(200)
			.then((res) => {
				expect(res.body).to.have.property('accessToken');
				user.setUserToken(res.body.accessToken);
				return done();
			})
			.catch(done);
	});
};