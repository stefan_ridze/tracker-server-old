const { expect, request } = require('../../');

module.exports = (user, report) => {
	it('POST /api/reports', (done) => {
		request.post('/api/reports')
			.set('token', user.getUserToken())
			.send(report)
			.expect(200)
			.then((res) => {
				expect(res.body).to.have.property('id');
				user.addNewReport(report);
				return done();
			})
			.catch(done);
	});
};