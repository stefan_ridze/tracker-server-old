const { expect, request } = require('../../');
const Config = require('../../../tests/system/config');

module.exports = (storage) => {
	const newUser = {
		email: Config.generateEmail(),
		password: Config.generatePassword(),
		fullName: Config.generateFullName(),
	};

	it('POST /api/users/sign-up', (done) => {
		request.post('/api/users/sign-up')
			.send(newUser)
			.expect(200)
			.then((res) => {
				expect(res.body).to.have.property('id');
				storage.setUser(newUser);
				return done();
			})
			.catch(done);
	});
};