const Service = require('.');
const moment = require('moment');

const {
	User,
} = require('../core/models');


class UserService extends Service {
	static getUserByEmail(email) {
		return this.findOne({ where: { email } });
	}

	static createUser(data) {
		return this.create(data);
	}

	static findUserById(userId) {
		return this.findOne({ where: { id: userId } });
	}

	static clockInUser(user, clockedInTime) {
		return user.update({ clockedInTime });
	}

	static clockOutUser(user) {
		return user.update({ clockedInTime: false });
	}
}

UserService.Model = User;

module.exports = UserService;
