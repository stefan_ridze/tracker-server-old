const bcrypt = require('bcrypt');
const uuidv4 = require('uuid/v4');
const Service = require('./index');

// Models
const {
	OAuthAccessToken,
} = require('../core/models');

// Services
const {
	UserSevice,
} = require('../core/services');

class OAuthAccessTokenService extends Service {
	static createToken(userId) {
		return this.create({
			accessToken: uuidv4(),
			userId,
		});
	}

	static findUserIdByAccessToken(accessToken) {
		return this.findOne({ where: { accessToken } });
	}

	static signOut(accessToken) {
		console.log('I got this as token:', accessToken);
		return this.destroy({ where: { accessToken } });
	}

	static async checkAuthorization(accessToken) {
		const user = await this.findUserIdByAccessToken(accessToken);
		return !!user;
	}
 }

OAuthAccessTokenService.Model = OAuthAccessToken;

module.exports = OAuthAccessTokenService;
