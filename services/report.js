const Service = require('.');
const moment = require('moment');

const {
	Report,
} = require('../core/models');

// const UnprocessableEntityError = require('../../core/exceptions/unprocessableEntity');
// const ForbiddenError = require('../../core/exceptions/forbidden');

function convertEntriesToReports(entries) {
	let result = [];
	// const days = []; optimize this, change reports to entries...
	entries.forEach(report => {
		const date = report.date;
		const dates = result.map(v => v.date);
		if (dates.includes(date)) {
			const index = dates.indexOf(date);
			result[index].dailyReports.push(report);

			// counting and formatting daily total
			let total = moment.duration(result[index].total);
			const sum = moment.duration(report.sum);
			total = moment.duration(total).add(sum);
			result[index].total = moment.utc(moment.duration(total).as('milliseconds')).format('HH:mm:ss');
		} else {
			result.push({
				date,
				dailyReports: [report],
				total: report.sum,
			});
		}
	});
	return result;
}


class ReportService extends Service {
	static createEntrie(data) {
		return this.create(data);
	}

	static async getReports(userId) {
		const entries = await this.findAll({
			where: { userId },
			order: [['updatedAt', 'DESC']],
		});
		return convertEntriesToReports(entries);
	}
}

ReportService.Model = Report;

module.exports = ReportService;
