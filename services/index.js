const Sequelize = require('sequelize');
const capitalizeFirstLetter = require('../helpers/capitalizeFirstLetter');

const NotFoundError = require('../core/exceptions/notfound');
const ForbiddenError = require('../core/exceptions/forbidden');
const BadRequestError = require('../core/exceptions/badrequest');
const InternalServerError = require('../core/exceptions/internalserver');

/**
 * @namespace Service
 * @property services Store selected service other service dependencies
 */
class Service {
	static initialize(services) {
		this.services = {};
		const servicesToLoad = this.dependencies;
		servicesToLoad.forEach((service) => {
			const fixedName = capitalizeFirstLetter(`${service}Service`);
			if (typeof services[fixedName] === 'undefined') {
				throw new InternalServerError('Cannot load service', `${fixedName} do not exists`);
			}
			this.services[fixedName] = services[fixedName];
		});
	}

	static async create(data, options) {
		return this.Model.create(data, options);
	}

	static async get(id, user, Model) {
		if (user) {
			return this.getForUser(id, user, Model);
		}

		const modelObject = (Model || this.Model);
		const modelData = await modelObject.findById(id);

		if (!modelData) {
			throw new NotFoundError('Entity not found', `${Model ? Model.name : this.Model.name} with the id ${id} not found`);
		}

		return modelData;
	}

	static async getForUser(id, user, Model) {
		const UserService = require('./user'); // eslint-disable-line global-require

		return this.get(id, null, Model)
			.then((model) => {
				if (!model) {
					throw new NotFoundError('Entity not found', `${Model ? Model.name : this.Model.name} with the id ${id} not found`);
				}

				if (model.isOwner(user) || user.isRoot()) {
					return model;
				}

				let userId = model.userId || false;
				if (model._modelOptions.name.singular === 'user') { // eslint-disable-line
					userId = model.id;
				}

				return UserService.isCustomerOwner(userId, user.referenceId)
					.then((value) => {
						if (value) {
							return model;
						}

						return Promise.reject(new ForbiddenError('Logged in user does not have permission to access selected object'));
					});
			});
	}

	static list(options) {
		return this.Model.findAndCountAll(options || {});
	}

	static findOne(options) {
		return this.Model.findOne(options);
	}

	static findAll(options) {
		return this.Model.findAll(options || {});
	}

	static update(data, options) {
		return this.Model.update(data, options);
	}

	static destroy(where) {
		return this.Model.destroy(where);
	}

	static get dependencies() {
		return [];
	}

	static findById(id, options) {
		return this.Model.findById(id, options);
	}
}

Service.Model = null;

module.exports = Service;