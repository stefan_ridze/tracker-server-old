const express = require('express');
const bcrypt = require('bcrypt');
const moment = require('moment');

// Exceptions
const NotFoundError = require('../../core/exceptions/notfound');
const BadRequestError = require('../../core/exceptions/badrequest');

// Middleware
const hasToken = require( '../../core/middleware/hasToken');

// Services
const {
	UserService,
	OAuthAccessTokenService,
	ReportService,
} = require('../../core/services');

const User = express.Router();

User.post('/users/sign-up', async (req, res, next) => {
	const {
		password,
		fullName,
		email,
	} = req.body;

	const saltRounds = 10;
	try {
		const hash = await bcrypt.hash(password, saltRounds);
		res.locals.data = await UserService.createUser({
			fullName,
			email,
			password: hash,
		});
		return next(null);
	}
	catch (error) {
		console.log(error);
		return next(error);
	}

});

User.post('/users/sign-in', async (req, res, next) => {
	const {
		email,
		password,
	} = req.body;
	console.log(req.body);
	try {
		const user = await UserService.getUserByEmail(email);
		const validatedPassword = await user.validatePassword(password);
		if (!user || !validatedPassword) {
			next(new NotFoundError('User is not found'));
		} else {
			const { accessToken } = await OAuthAccessTokenService.createToken(user.id);
			user.accessToken = accessToken;
			res.locals.data = user;
			return next(null);
		}
	}
	catch (error) {
		return next(error);
	}
});

User.post('/users/sign-out', hasToken, async (req, res, next) => {
	try {
		const { accessToken } = res.locals;
		await OAuthAccessTokenService.signOut(accessToken);
		res.locals.data = {
			logout: true,
		};
		return next();
	}
	catch (error) {
		return next(error);
	}
});

User.get('/users/check-authorization', async (req, res, next) => {
	try {
		if (res.locals.accessToken) {
			res.locals.data = {
				authorized: true,
			}
		} else {
			res.locals.data = {
				authorized: false,
			}
		}
		return next(null);
	}
	catch (error) {
		return next(error);
	}
});

User.put('/users/clock-in', hasToken, async (req, res, next) => {
	const userId = res.locals.user.id;
	const { clockedInTime } = req.body;

	try {
		// Validating clockedInTime it shouldn't be older then a few seconds or be after now (in future)
		const now = moment();
		const limiter = moment().subtract(5, 'seconds').format();
		if (!moment(clockedInTime).isValid() || moment(clockedInTime).isBefore(limiter) || moment(now).isBefore(clockedInTime)) {
			return next(new BadRequestError(null, 'clockedInTime is not valid'));
		}

		const user = await UserService.findUserById(userId);
		await UserService.clockInUser(user, clockedInTime);
		res.locals.data = {
			clockedInTime: user.clockedInTime,
		};
		return next(null);
	}
	catch (error) {
		return next(error);
	}
});

User.put('/users/clock-out', hasToken, async (req, res, next) => {
	const { clockedOutTime } = req.body;
	const { id: userId } = res.locals.user;

	console.log('CLOCKED OUT TIME', clockedOutTime);
	// TODO Validate clockedOutTime

	try {
		const user = await UserService.findUserById(userId);
		const { clockedInTime } = user;
		const date = moment().format('ll');

		// counting duration length
		const duration = moment.duration(moment(clockedInTime).diff(moment(clockedOutTime)));
		const milliseconds = Math.abs(duration.as('milliseconds'));
		const sum = moment.utc(milliseconds).format('HH:mm:ss');

		ReportService.createEntrie({
			clockedInTime: moment(clockedInTime).format('HH:mm:ss'),
			clockedOutTime: moment(clockedOutTime).format('HH:mm:ss'),
			date,
			sum,
			userId,
		});
		await UserService.clockOutUser(user);
		return next(null);
	}
	catch (error) {
		return next(error);
	}
});

module.exports = User;
