const express = require('express');
const hasToken = require( '../../core/middleware/hasToken');

// Services
const {
	ReportService,
} = require('../../core/services');

const Report = express.Router();

// Submit the report
Report.post('/reports', hasToken, async (req, res, next) => {
	const { id } = res.locals.user;
	try {
		res.locals.data = await ReportService.createEntrie({
			...req.body,
			userId: id,
		 });
		 return next(null);
	} catch (error) {
		return next(error);
	}
});

// Get all Reports of a particular user
Report.get('/reports', hasToken, async (req, res, next) => {
	console.log('GET REPORTS');
	const { id } = res.locals.user;
	try {
		res.locals.data = await ReportService.getReports(id);
		return next(null);
	} catch (error) {
		return next(error);
	}
});

module.exports = Report;
