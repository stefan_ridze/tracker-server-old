const Sequelize = require('sequelize');
const sequelize = require('../../core/database/mysql');
const bcrypt = require('bcrypt');
const moment = require('moment');

/**
 * @namespace User
 * @class
 * @type {Object}
 */
const User = sequelize.define(
	'user',
	{
		id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV4,
			primaryKey: true,
		},
		email: {
			type: Sequelize.STRING,
			allowNull: false,
			validate: {
				isEmail: true,
			},
			unique: 'user_unique_email',
		},
		fullName: {
			type: Sequelize.STRING,
			allowNull: false,
			validate: {
				len: [1, 255],
			},
		},
		password: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		accessToken: {
			type: Sequelize.STRING,
			allowNull: true,
		},
		clockedInTime: {
			type: Sequelize.STRING,
			defaultValue: false,
			allowNull: true,
			// TODO add validation
		},

	}
);

User.prototype.validatePassword = function validatePassword(password) {
	return bcrypt.compare(password, this.password);
};

module.exports = User;
