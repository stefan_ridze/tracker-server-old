const Sequelize = require('sequelize');
const sequelize = require('../../core/database/mysql');

/**
 *
 * @type {Object}
 */
const Report = sequelize.define(
	'report',
	{
		id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV4,
			primaryKey: true,
		},
		clockedInTime: {
			type: Sequelize.STRING,
			allowNull: false,
			// TODO add validation
		},
		clockedOutTime: {
			type: Sequelize.STRING,
			allowNull: false,
			// TODO add validation
		},
		sum: {
			type: Sequelize.STRING,
			allowNull: false,
			// TODO add validation
		},
		date: {
			type: Sequelize.STRING,
			allowNull: false,
			validate: {
				isDate: true,
			},
		},
	}
);

Report.associate = (models) => {
	const {
		User
	} = models;
	Report.belongsTo(User);
	User.hasMany(Report);
};

module.exports = Report;
