const Sequelize = require('sequelize');
const sequelize = require('../core/database/mysql');

const AccessToken = sequelize.define(
	'oAuthAccessToken',
	{
		accessToken: Sequelize.STRING, // index this
		refreshToken: Sequelize.STRING, // index this
		accessTokenExpiresAt: Sequelize.DATE,
		refreshTokenExpires: Sequelize.DATE,
	},
	// {
	// 	indexes: [
	// 		{
	// 			fields: ['accessToken'],
	// 		},
	// 		{
	// 			fields: ['refreshToken'],
	// 		},
	// 	],
	// }
);

AccessToken.associate = (db) => {
	const {
		User,
	} = db;

	AccessToken.belongsTo(User);
};

module.exports = AccessToken;
