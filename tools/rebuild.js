const models = require('../core/models');

(async function rebuild() {
	try {
		await models.sequelize.sync({
			force: true,
			// logging: console.log,
		});
		process.exit(0);
	}
	catch (error) {
		console.log(error);
	}
}());
